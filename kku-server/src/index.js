require('dotenv').config()

const Koa = require('koa')
const Router = require('koa-router')

const app = new Koa()
const router = new Router()
const api = require('./api')

const mongoose = require('mongoose')
const bodyParser = require('koa-bodyparser')

mongoose.Promise = global.Promise // Node 의 네이티브 Promise 사용
// mongodb 연결
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(
    (response) => {
        console.log('Successfully connected to mongodb')
    }
).catch(e => {
    console.error(e)
})

const port = process.env.PORT || 3000

app.use(bodyParser()) // 바디파서 적용, 라우터 적용코드보다 상단에 있어야한다.

// app.use(async ctx => {
//     // 아무것도 없으면 {} 가 반환된다.
//     ctx.body = ctx.request.body
// })
// router.get('/', (ctx, next) => {
//     // ctx.redirect('http://kkuisland.com:3000')
//     ctx.body='Test'
// })


// router.get('/about', (ctx, next) => {
//     ctx.body='소개'
// })

// router.get('/about/:name', (ctx, next) => {
//     const { name } = ctx.params // 라우트 경로에서 :파라미터명 으로 정의된 값이 ctx.params 안에 설정된다.
//     ctx.body = name + '의 소개'
// })

// router.get('/post', (ctx, next) => {
//     const { id } = ctx.request.query // 주소 뒤에 ?id=10 이런 식으로 작서된 쿼리는 ctx.request.query 에 파싱됩니다.
//     if( id ) {
//         ctx.body = '포스트 #' + id
//     } else {
//         ctx.body = '포스트 아이디가 없습니다.'
//     }
// })

router.use('/api', api.routes())

app.use(router.routes()).use(router.allowedMethods())

app.listen(port, () => {
    console.log('KKu Island server is listening to port 3000!!')
})