const mongoose = require('mongoose')
const { Schema } = mongoose

// Book 에서 사용할 스키마입니다
const blogSchema = new Schema({
    title: String,
    author: String,
    body: String,
    comments: [{ body: String, date: Date }],
    date: { type: Date, default: Date.now },
    hidden: Boolean,
    meta: {
        votes: Number,
        favs: Number
    }
})

// 모델
const Blog = mongoose.model('Blog', blogSchema)
