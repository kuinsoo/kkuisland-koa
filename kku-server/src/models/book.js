const mongoose = require('mongoose')
const { Schema } = mongoose

// Book 스키마
const Author = new Schema({
    name: String,
    email: String
})

const Book = new Schema({
    title: String,
    authors: [Author],
    publishedDate: Date,
    price: Number,
    tags: [String],
    createdAt: {
        type: Date,
        default: Date.now // 기본값은 현재 날짜로 지정
    }
})

// 스키마를 모델로 변환 
module.exports = mongoose.model('Book', Book)